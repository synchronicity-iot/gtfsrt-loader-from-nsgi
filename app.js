// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const config = require("./config");

process.env.LOG = config.logLevel;
process.env.SUPPRESS_NO_CONFIG_WARNING = 'y';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const http = require('http');
const bodyParser = require('body-parser');
const express = require('express');
const log = require('./src/utils/logger');
const ngsiSubs = require('./src/utils/ngsiSubs');

process.stdin.resume();//so the program will not close instantly
async function exitHandler(options, exitCode) {
  log.info('Service going down');
  await ngsiSubs.cleanSubs().catch (e => {
    console.log(e);
  });
  process.exit();
}

// process.on('exit', exitHandler.bind(null,{}));
//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {}));
process.on('SIGTERM', exitHandler.bind(null, {}));
// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {}));
process.on('SIGUSR2', exitHandler.bind(null, {}));
//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {}));

const gtfsrtRefresh = config.gtfsrtRefresh || 300;
setInterval(async () => {
  await require('./src/routines/resetFeeds').updateTripUpdateFeed();
}, gtfsrtRefresh*1000);

const setupServer = async () => {
  const app = express();
  const admin = express();
  const router = express.Router();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use( async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    return next();
  });
  app.use('/', router);
  require('./src/routes/service.js') (app);
  const port = config.httpPort || 8080;
  const httpServer = http.createServer(app);
  httpServer.listen(port);
  log.info('Application HTTP Server listening in PORT ' + port);

  admin.use(bodyParser.urlencoded({ extended: true }));
  admin.use(bodyParser.json());

  admin.use( async (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    return next();
  });
  admin.use('/', router);
  require('./src/routes/management.js') (admin);
  const port_admin = config.httpPort_admin || 8081;
  process.env.NOTIFY_ENDPOINT = (config.notifyProtocol || 'http') + '://' + config.notifyAddress + ':' + port_admin + '/notify';
  const httpServer_admin = http.createServer(admin);
  httpServer_admin.listen(port_admin);
  log.info('Management HTTP Server listening in PORT ' + port_admin);
}
setupServer();