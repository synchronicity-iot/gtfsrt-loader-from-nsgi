# GTFS-RT loader from NGSI

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

> GTFS-RT feeder from [ArrivalEstimation](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/ArrivalEstimation/doc/spec.md) and [GtfsTransitFeedFile](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/GtfsTransitFeedFile/doc/spec.md) NGSI entities. 

This service uses [ArrivalEstimation](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/ArrivalEstimation/doc/spec.md) information for:

1. Creating and serving [TripUpdate](https://developers.google.com/transit/gtfs-realtime/reference/#message-tripupdate) [GTFS-RT](https://developers.google.com/transit/gtfs-realtime/reference/) feeds. Other [GTFS-RT](https://developers.google.com/transit/gtfs-realtime/reference/) could be added in the future, provided there are NGSI entities datamodels that allow it.
2. Associating [GTFS-RT](https://developers.google.com/transit/gtfs-realtime/reference/) feeds with static [GTFS](https://gtfs.org/) feeds referenced by [GtfsTransitFeedFile](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/GtfsTransitFeedFile/doc/spec.md) entities.

# Service description

The service generates GTFS-RT feeds for `TripUpdates` from NGSI entities of type `ArrivalEstimation`. The service supports the creation of several GTFS-RT feeds, by grouping `ArrivalEstimation` entities referred to the same `GtfsTransitFeedFile` (GTFS feed) indicated in the attribute `refGtfsTransitFeedFile`. See [ArrivalEstimation](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/ArrivalEstimation/doc/spec.md) and [GtfsTransitFeedFile](https://gitlab.com/synchronicity-iot/synchronicity-data-models/blob/master/UrbanMobility/GtfsTransitFeedFile/doc/spec.md) datamodels for more information.

This is done in two steps:

1. Generation of ORION subscriptions for `ArrivalEstimation` entities referring to the same `GtfsTransitFeedFile`. 
2. Caching of updates of the `ArrivalEstimation` entities from ORION notifications.
3. Dumping of cached information to GTFS-RT binary files.
4. Serving of those GTFS-RT binary files 

This service exposes two interfaces: management and service APIs. The first one is intended to manage the service configuration and gather information. The second one is used to consume the GTFS-RT feeds.

# Management API

* **POST** `/management/feed/:gtfsFeedName`: Creation of new GTFS-RT feed corresponding to a specific `GtfsTransitFeedFile` indicated by the `gtfsFeedName` parameter.
* **GET** `/management/feed/:gtfsFeedName`: Retrieve stored information corresponding to a specific `GtfsTransitFeedFile`.
* **DELETE** `/management/feed/:gtfsFeedName`: Remove stored information corresponding to a specific `GtfsTransitFeedFile`. Note that new information is generated when updates are recived.  
* **POST** `/management/loadFeed/:gtfsFeedName`: Force asynchronous update of the GTFS-RT binary file corresponding to a `GtfsTransitFeedFile`.
  
* **GET** `/management/feedlist`: Retrieve list of all `GtfsTransitFeedFile` for which GTFS-RT files are being generated.
* **GET** `/management/feeds`: Retrieve stored information of all `GtfsTransitFeedFile`.  
* **DELETE** `/management/feeds`: Remove all `GtfsTransitFeedFile`. It also removes subscriptions in ORION and binary files.
* **POST** `/management/loadFeeds`: Force asynchronous update of all GTFS-RT binary files corresponding to the `GtfsTransitFeedFile`.
* **POST** `/notify` : ORION notification with `ArrivalEstimation` updates.

# Service API

* **GET** `/gtfsrt/:gtfsFeedName`: GTFS-RT binary file for a specific `GtfsTransitFeedFile`.

# Configuration

The service configuration is done through the file `config.js`, where the following parameters are defined:

* `logLevel`: loging level.
* `httpPort`: port of the service API (8080 by default).
* `httpPort_admin`: port of the management API (8081 by default).
* `tripUpdatesProtoPath`: path of the proto file `TripUpdates`
* `fiwareService`: Fiware service if necessary, if not set default is used
* `fiwareServicePath`: Fiware service path if necessary, if not set default is used
* **`contextBroker`**: URL of the ORION context broker to generate the subscriptions (**mandatory**).
* `notifyProtocol`: protocol to notify (`http` by default).
* **`notifyAddress`**: IP address or doamin to notify (**mandatory**). The service will add `httpPort_admin` to send the notification.
* `gtfsrtRefresh`: refresh periodicity of the GTFS-RT binary files in seconds (300 by default).
* `notifyThrottling`: ORION notify throttling in seconds (30 by default).

# Installation

The service installation can be done either as a native service or using Docker.

## Normal (Native node JS)

The service has been developed and tested using nodejs version 11.4.0. You can manage the nodejs version using [nvm](https://nodejs.org/en/download/package-manager/).

Once nodejs installed in your system, just go to the service folder
```
cd gtfsrt-loader-from-ngsi
```
Create your `config.js` with your own setup (see above). You can reuse the examples configuration in `config.js.example`:
```
cp config.js.example config.js
```
And install the dependencies
```
npm install
```
Finally you can run the service manually
```
node app.js
```
using [pm2](http://pm2.keymetrics.io/) to keep it running
```
pm2 start pm2.json
```
or in developing mode to get the service restarted when the code is modified
```
npm start
```

## Docker 

Create your `config.js` with your own setup (see above). You can reuse the examples configuration in `config.js.example`:
```
cp config.js.example config.js
```

Execute the `docker-compose` file. You can change the ports forwarding and the loation of the `config.js` in this file.
```
docker-compose up -d
```

## Tests

In the following we show some CURL examples for testing and using the service. Note that only steps 1, 6 and, maybe, 7 would be use in a regular deployment.

*For your convenience a [Postman](https://www.getpostman.com/) is included in the file with all the test queries*

Let's assume that the service is running in the URL `gtfsrtService.synchronicity-iot.eu` with default paramters: management API is at `gtfsrtService.synchronicity-iot:8081`, and service API at `gtfsrtService.synchronicity-iot.eu:8080`.

1. Register a new `GtfsTransitFeedFile`

```
curl -X POST http://gtfsrtService.synchronicity-iot:8081/management/feed/urn:ngsi-ld:GtfsTransitFeedFile:Santander:bus 
``` 

At this point a new subscription is generated in the context management entity for entities of type `ArrivalEstimation` and whose attribute `refGtfsTransitFeedFile` is equal to `urn:ngsi-ld:GtfsTransitFeedFile:Santander:metro`.

2. Check the `GtfsTransitFeedFile` registered

```
curl -X GET http://gtfsrtService.synchronicity-iot:8081/management/feedList 
```

3. Notify from context broker

This step is accomplished by the context broker in a regular deployment.

```
curl -X POST http://gtfsrtService.synchronicity-iot:8081/notify \
  -H 'Content-Type: application/json' \
  -d '{
  "data": [
  {
    "id": "urn:ngsi-ld:ArrivalEstimation:Santander:bus:1",
    "type": "ArrivalEstimation",
    "refGtfsTransitFeedFile": "urn:ngsi-ld:GtfsTransitFeedFile:Santander:bus",
    "arrivalEstimationUpdate": [{
      "tripId": "0",
      "arrivalDelay": -1,
      "vehicleId": "bus1",
    }, {
      "tripId": "1",
      "arrivalDelay": -2,
      "vehicleId": "bus2",
    }],
    "routeId": "route3",
    "stopId": "stop1",
    "lastUpdatedAt": "2016-12-19T10:15:00.00Z"
  }]}'
``` 

4. Check the cached information 

```
curl -X GET http://gtfsrtService.synchronicity-iot:8081/management/feed/urn:ngsi-ld:GtfsTransitFeedFile:Santander:bus
```
5. Dump to binary file 

This step is automatically performed by the service in a regular deployment, according to the value of the configuration parameter `gtfsrtRefresh`.

You can dump all registered `GtfsTransitFeedFile` 

```
curl -X POST http://gtfsrtService.synchronicity-iot:8081/management/loadFeeds 
```

or just one 

```
curl -X POST http://gtfsrtService.synchronicity-iot:8081/management/loadFeed/urn:ngsi-ld:GtfsTransitFeedFile:Santander:bus
```

6. Retrieve a GTFS-RT file

```
curl -X GET http://gtfsrtService.synchronicity-iot:8080/gtfsrt/urn:ngsi-ld:GtfsTransitFeedFile:Santander:bus
```

7. Clean all

```
curl -X DELETE http://gtfsrtService.synchronicity-iot:8081/management/feeds
```
# Support
Any feedback on this documentation is highly welcome, including bugs, typos and suggestions.

The recommended way to provide feedback and receive support is to use repository [Issues](https://gitlab.com/synchronicity-iot/gtfsrt-loader-from-nsgi/issues) and [Merge Requests](https://gitlab.com/synchronicity-iot/gtfsrt-loader-from-nsgi/merge_requests).

# About 
For more information, check the project's site [here](https://synchronicity-iot.eu/)

# License
This atomic service is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. Please refer to gnu licenses for more information.

The software is released as it is and we discharge any liability.