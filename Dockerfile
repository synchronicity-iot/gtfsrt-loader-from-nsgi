FROM node:10.15.3-alpine

WORKDIR /home/node

RUN apk add --update npm \
  && npm install -g pm2 \
  && apk add git \ 
  && git clone --depth=1 https://gitlab.com/synchronicity-iot/gtfsrt-loader-from-nsgi.git \ 
  && cd gtfsrt-loader-from-nsgi \
  && npm install

WORKDIR /home/node/gtfsrt-loader-from-nsgi

CMD ["pm2-runtime", "start", "app.js"]
