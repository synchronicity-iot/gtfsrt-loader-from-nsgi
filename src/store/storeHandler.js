// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const log = require('../utils/logger');
const _  = require('lodash');
const updatesH = require ('./updatesHandler')
const alertsH = require ('./alertsHandler')
const positionsH = require ('./positionsHandler')

const processNotification = async (data) => {
  if (!_.isArray(data)) {
    if (data.type === 'ArrivalEstimation') {
      updatesH.processNotification(data);
    } else {
      log.error ('Type ' + data.type + ' is not recognized to create a GTFS-RT feed');
    }
    return;
  }
  _.forEach(data, u => {
    if (u.type === 'ArrivalEstimation') {
      updatesH.processNotification(u);
    } else {
      log.error ('Type ' + u.type + ' is not recognized to create a GTFS-RT feed');
    }
  })
}

const query = (gtfsFeedId, feedName, time) => {
  if (feedName === 'TripUpdates') {
    return updatesH.query(gtfsFeedId, time);
  } else if (feedName === 'ServiceAlerts') {
    return alerstH.query(gtfsFeedId, time);
  } else if (feedName === 'VehiclePositions') {
    return positionstH.query(gtfsFeedId, time);
  }
  log.error ('Update ' + feedName + ' is not recognized to create a GTFS-RT feed');
}

const queryAll = (feedName) => {
  if (feedName === 'TripUpdates') {
    return updatesH.queryAll();
  } else if (feedName === 'ServiceAlerts') {
    return alerstH.queryAll();
  } else if (feedName === 'VehiclePositions') {
    return positionstH.queryAll();
  }
  log.error ('Update ' + feedName + ' is not recognized to create a GTFS-RT feed');
}

const removeByTime = (gtfsFeedId, feedName, time) => {
  if (feedName === 'TripUpdates') {
    return updatesH.removeByTime(gtfsFeedId, time);
  } else if (feedName === 'ServiceAlerts') {
    return alerstH.removeByTime(gtfsFeedId, time);
  } else if (feedName === 'VehiclePositions') {
    return positionstH.removeByTime(gtfsFeedId, time);
  } 
  log.error ('Update ' + feedName + ' is not recognized to create a GTFS-RT feed');
}

const removeAllByTime = (feedName, time) => {
  if (feedName === 'TripUpdates') {
    return updatesH.removeAllByTime(time);
  } else if (feedName === 'ServiceAlerts') {
    return alerstH.removeAllByTime(time);
  } else if (feedName === 'VehiclePositions') {
    return positionstH.removeAllByTime(time);
  } 
  log.error ('Update ' + feedName + ' is not recognized to create a GTFS-RT feed');
}

const removeAll = (feedName) => {
  if (feedName === 'TripUpdates') {
    return updatesH.removeAll();
  } else if (feedName === 'ServiceAlerts') {
    return alerstH.removeAll();
  } else if (feedName === 'VehiclePositions') {
    return positionstH.removeAll();
  } 
  log.error ('Update ' + feedName + ' is not recognized to create a GTFS-RT feed');
}

const list = (feedName) => {
  if (feedName === 'TripUpdates') {
    return updatesH.list();
  } else if (feedName === 'ServiceAlerts') {
    return alerstH.list();
  } else if (feedName === 'VehiclePositions') {
    return positionstH.list();
  }
  log.error ('Update ' + feedName + ' is not recognized to create a GTFS-RT feed');
}

module.exports = {
  /**
   * It updates the entries in the database affected by an ORION notification, using the new timestamp indicated by the attribute lastUpdatedAt 
   */
  processNotification: processNotification,
  /** 
   * It returns the records corresponding to a specific feed that have been updated after the given time
   */
  query : query,
  /** 
   * It returns all the storage of one Feed Type
   */
  queryAll : queryAll,
  /** 
   * It removes the entries from the data base corresponding to a specific feed whose last updated is before the given time 
   */
  removeByTime : removeByTime,
  removeAllByTime : removeAllByTime,
  /** 
   * It removes all the storage of one Feed Type
   */
  removeAll : removeAll,
  /**
   * List all the feeds of a given type
   */
  list: list
}