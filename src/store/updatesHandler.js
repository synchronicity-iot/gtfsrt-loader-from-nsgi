// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const log = require('../utils/logger');
const _  = require('lodash');
const prettyjson = require('prettyjson');

var storage = {};

const checkData = (d) => { 
  return _.has(d, 'refGtfsTransitFeedFile');
}

const doProcessUpdates = (d) => {
  if (!checkData(d)) { // silently ignore
    log.error('Malformed data');
    return;
  } 

  const stopId = d.stopId;
  const gtfsFeedId = d.refGtfsTransitFeedFile;
  if (!_.has(storage, gtfsFeedId)) {
    storage[gtfsFeedId] = [];
  }

  _.forEach (d.arrivalEstimationUpdate , arrivalUpdate => {
    const key = arrivalUpdate.tripId + ':' +  d.routeId;
    const arrivalUpdateData = {
      stopId : stopId,
      arrival: {}
    }

    if (_.has(d, 'stopSequence')) {
      arrivalUpdateData['stopSequence'] = arrivalUpdate.stopSequence;
    }    
    if (_.has(arrivalUpdate, 'arrivalDelay')) {
      arrivalUpdateData['arrival']['delay'] = arrivalUpdate.arrivalDelay;
    }
    if (_.has(arrivalUpdate, 'arrivalTime')) {
      arrivalUpdateData['arrival']['time'] = arrivalUpdate.arrivalTime;
    }

    const vehicleData = {}
    if (_.has(arrivalUpdate, 'vehicleId')) {
      vehicleData['vehicleId'] = arrivalUpdate.vehicleId;
    }
    if (_.has(arrivalUpdate, 'vehicleLabel')) {
      vehicleData['vehicleLabel'] = arrivalUpdate.vehicleLabel;
    }
    const keyIdx = _.findIndex(storage[gtfsFeedId], { 'ID': key}) 
    if (keyIdx == -1) {
      storage[gtfsFeedId].push ({
        ID: key,
        tripUpdate: {
          trip : {
            tripId: arrivalUpdate.tripId,
            routeId: d.routeId
          },
          stopTimeUpdate: [arrivalUpdateData],
          vehicle: vehicleData,
          timestamp: d.lastUpdatedAt
        }
      });
    } else {
      const idx = _.findIndex(storage[gtfsFeedId][keyIdx].tripUpdate.stopTimeUpdate, { 'stopId': arrivalUpdateData.stopId });
      if (idx !== -1) {
        storage[gtfsFeedId][keyIdx].tripUpdate.stopTimeUpdate[idx] = arrivalUpdateData;
      } else {
        storage[gtfsFeedId][keyIdx].tripUpdate.arrivalUpdate.push(arrivalUpdateData);
      }
      storage[gtfsFeedId][keyIdx].tripUpdate.vehicle = vehicleData;
      storage[gtfsFeedId][keyIdx].tripUpdate.timestamp = d.lastUpdatedAt
    }
  })
}

const processNotification = async (data) => {
  log.info('Processing entity ' + data.id)
  doProcessUpdates(data);
}

const query = (gtfsFeedId, time) => {
  if (!_.has(storage, gtfsFeedId)) {
    log.warn('GTFS FeedId ' + gtfsFeedId + ' is not recorded');
    return {};
  }
  return storage[gtfsFeedId];
}

const queryAll = () => {
  return storage; 
}

const removeByTime = async (gtfsFeedId, time) => {
  if (!_.has(storage, gtfsFeedId)) {
    return;
  }
  storage[gtfsFeedId] = _.filter(storage[gtfsFeedId], o => {
    const tr = new Date(o.tripUpdate.timestamp);
    const tb = new Date(time);
    return (tr > tb)
  });

}

const removeAllByTime = async (time) => {
  _.forOwn(storage, (v, k) => { 
    removeByTime(k, time);
  });
}

const removeAll = () => {
  storage = {};
}

const list = () => {
  const list = [];
  _.forOwn(storage, (v,k) => {
    list.push(k);
  })
  return list;
}

module.exports = {
  processNotification: processNotification,
  query : query,
  queryAll : queryAll,
  removeByTime : removeByTime,
  removeAllByTime : removeAllByTime,
  removeAll : removeAll,
  list : list
}