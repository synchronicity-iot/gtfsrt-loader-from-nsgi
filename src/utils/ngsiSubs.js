// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const _ = require("lodash");
const axios = require("axios");
const log = require("../utils/logger");
const config = require("../../config");
const fs = require("fs-extra");
const prettyjson = require("prettyjson");

const init = async () => {
  await fs.mkdir('./cache', { recursive: true });
}

const getBodyTemplate = () => {
  const d = new Date(); //.setFullYear(new Date().getFullYear() + 1));
  d.setFullYear(d.getFullYear() + 1);
  return {
    description: "",
    subject: {
      entities: [
        {
          idPattern: ".*",
          type: ""
        }
      ],
      condition: {
        attrs: ["lastUpdatedAt"],
        expression: { q: "" }
      }
    },
    notification: {
      http: { url: process.env.NOTIFY_ENDPOINT },
      attrs: [],
      attrsFormat: "keyValues"
    },
    expires: d.toISOString(),
    throttling: config.notifyThrottling || 30
  };
};

const getBodyTripUpdates = (gtfsFeedId) => {
  const ret = getBodyTemplate();
  ret.description =
    "NSGI/GTFS-RT loader subscription for TripUpdates of " + gtfsFeedId;
  ret.subject.entities[0].type = "ArrivalEstimation";
  ret.subject.condition.expression.q = "refGtfsTransitFeedFile==" + gtfsFeedId;
  ret.notification.attrs = [    
    "refGtfsTransitFeedFile",
    "routeId",
    "stopId",
    "lastUpdatedAt",
    "arrivalEstimationUpdate"
  ]; 
  return ret;
};

const getBodyServiceAlerts = async (gtfsFeedId) => {};

const getBodyVehiclePositions = async (gtfsFeedId) => {}; 

const cacheSubId = async (id) => {
  let obj = {};
  const rd = await fs.readFile('./cache/subs.json', 'utf8').catch (e => {
    obj = {subscriptions: [id]};
  })
  if (rd !== undefined) {
    obj = JSON.parse(rd);
    if (!_.isArray(obj.subscriptions)) {
      obj = {subscriptions: [id]};
    } else {
      obj.subscriptions.push(id)
    }
  }
  await fs.writeFile('./cache/subs.json', JSON.stringify(obj, null, 2)).catch (e => {});
}

const getCachedSubids = async (id) => {
  let obj = {};
  const rd = await fs.readFile('./cache/subs.json', 'utf8').catch (e => {
    obj = {subscriptions: []};
  })
  if (rd !== undefined) {
    obj = JSON.parse(rd);
  }
  
  return obj.subscriptions;
}

const cleanCachedSubIds = async () => {
  await fs.writeFile('./cache/subs.json', JSON.stringify({subscriptions: []})).catch (e => {
    console.error(e)
  });
}

const createSub = async (feedName, gtfsFeedId) => {
  log.info ('Creating subscription for ' + feedName + ' ==> ' + gtfsFeedId);
  const body = feedName  === 'TripUpdates' ? getBodyTripUpdates(gtfsFeedId) : 
  feedName === 'ServiceAlerts' ? getBodyServiceAlerts (gtfsFeedId) : 
  getBodyVehiclePositions (gtfsFeedId);
  const fs = config.fiwareService || '';
  const fsp = config.fiwareServicePath || '';
  const options = {
    method: "post",
    url: config.contextBroker + "/v2/subscriptions/",
    data: body,
    headers: {
      'fiware-service':fs,
      'fiware-servicePath':fsp
    }
  };

  const res = await axios(options).catch (e => {
    console.error(e)
  });
  if (res) {
    cacheSubId (res.headers.location);
    log.info ('Cached subscription ' + res.headers.location);
  }  
};

const cleanSubs = async opt => {
  log.info ('Removing cached subscriptions');
  const subs = await getCachedSubids();
  const fs = config.fiwareService || '';
  const fsp = config.fiwareServicePath || '';
  const options = {
    method: "delete",
    url: '',
    headers: {
      'fiware-service':fs,
      'fiware-servicePath':fsp
    }
  };
  for (const sub of subs) {
    options.url = config.contextBroker + sub;
    log.info('Removing subscription ' + options.url)
    await axios(options).catch(e => {
      log.error (options.url + ' ==> ' + e.response.data.description)
    });
    
  }
  await cleanCachedSubIds();
  log.info ('Subscriptions removed');
};

init ();
module.exports = {
  createSub: createSub,
  cleanSubs: cleanSubs
};
