// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by 
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const ProtoBuf = require('protobufjs');
const fs = require('fs-extra');
const log = require('./logger');
const prettyjson = require('prettyjson');

const Root  = ProtoBuf.Root;
const Type  = ProtoBuf.Type;
const Field = ProtoBuf.Field;
const Util = ProtoBuf.util;
/*

*/
const Init = async (protoPath) => {
  log.info('Loading Gtfs Reatlime proto description from ' + protoPath);
  const GtfsRtProto = await ProtoBuf.load(protoPath).catch (e => {
    throw(e);
  });
}

const Verify = (msgHandler, payload) => {
  // console.log(prettyjson.render(payload))     
  const err = msgHandler.verify(payload);
  if (err) {
    console.error(err);
    return;
  }
  log.info ('Payload successfully verfied!!');
}

const WriteToFile = async (msgHandler, payload, fn) => {
  const msg = msgHandler.create(payload);
  var buffer = msg.encode(msg).finish();
  const ret = await fs.writeFile('fn', buffer,  'binary'); // it can throw
}

const DevCreate = async (protoPath, payload) => {
  
  log.info('Loading Gtfs Reatlime proto description from ' + protoPath);
  const gtfsRtProto = await ProtoBuf.load(protoPath).catch (e => {
    log.error (e)
    throw(e);
  });
  const msgH = gtfsRtProto.lookupType('transit_realtime.FeedMessage');
  Verify(msgH, payload);
  // const msg = msgH.create(payload);
  // console.log(Util.isset(msg, 'header'));
  //console.log(msg.$type);
  // console.log(msg.toJSON())
  // 
  
}

const AddEntity = () => {

}


module.exports = {
  Init: Init,
  DevCreate: DevCreate,
  AddEntity: AddEntity, 
  
}