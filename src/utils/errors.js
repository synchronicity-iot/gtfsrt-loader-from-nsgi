// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const _ = require('lodash');

const urlUnspec = {
  'type': '',
  'title': 'URL unspecified',    
  'detail': 'You need to specify the URL the context broker is listening at including the protocol'
};

const typeUnspec = {
  'type': '',
  'title': 'Type unspecified',    
  'detail': 'You need to specify the TYPE of the entities you are validating'
};

const entityUnspec = {
  'type': '',
  'title': 'Entity unspecified',    
  'detail': 'You need to specify the entity ID'
};

const attributeUnspec = {
  'type': '',
  'title': 'Attribute unspecified',    
  'detail': 'You need to specify attribute name'
};

const getErrorMsg = (e) => {
  try {
    return {url: e.config.url, status: e.response.status, msg: e.response.statusText};    
  } catch (er) {
    try {
      return {url: e.config.url, status: 'unknown error'};    
    } catch (er) {
      return 'unknown error';    
    }
  }
}

const getErrorMsg2 = (e) => {
  try {
    return {url: e.config.url, status: e.response.status, msg: e.response.statusText};    
  } catch (e) {
    return 'unknown error';
  }
}

module.exports = {
  urlUnspec: urlUnspec,
  typeUnspec: typeUnspec, 
  entityUnspec: entityUnspec, 
  attributeUnspec: attributeUnspec, 
  getErrorMsg: getErrorMsg,
  getErrorMsg2: getErrorMsg2
};