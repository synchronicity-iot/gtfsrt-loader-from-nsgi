// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

const myFormat = printf(msg => {
  return `[${msg.timestamp} [${msg.label}] ${msg.level}: ${msg.message}`;
});

const transport = new (winston.transports.DailyRotateFile)({
  dirname: './logs',
  filename: 'serv.log',
  datePattern: 'YYYY-MM-DD',
  maxsize: '5mb', //5MB,
  zippedArchive: true
}); 

const transportErr = new (winston.transports.DailyRotateFile)({
  dirname: './logs',
  level: 'silly',
  filename: 'error.log',
  datePattern: 'YYYY-MM-DD',
  maxsize: '5mb', //5MB,
  zippedArchive: true
});

logger = winston.createLogger({  
  level: process.env.LOG,
  format: combine(
    label({ label: __filename.replace(/^.*[\\\/]/, '') }),
    timestamp(),
    myFormat
  ),
  transports: [
    transportErr,
    transport    
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
  format: combine(
    winston.format.colorize({message: true}),
    label({ label: 'gtfsrt-ngsi-loader' }),
    timestamp(),
    myFormat
  ),
  }));
}

module.exports = logger;