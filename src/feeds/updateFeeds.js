// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const log = require("../utils/logger");
const _ = require("lodash");
const storeH = require("../store/storeHandler");
const prettyjson = require("prettyjson");
const ProtoBuf = require("protobufjs");
const fs = require("fs-extra");
const path = require('path');
const config = require("../../config");

const dirPath = './feeds/TripUpdate/';

const header = {
  gtfsRealtimeVersion: "1.0"
};

let tripUpdatesProto;

const init = async () => {
  await fs.mkdir(dirPath, { recursive: true });
  const protoPath = config.tripUpdatesProtoPath || './protos/gtfs-realtime.proto';
  tripUpdatesProto = await ProtoBuf.load(protoPath).catch(
    e => {
      console.error(e)
      return;
    }
  );
};

const convertDates = d => {
  const ret = [];
  _.forEach(d, e => {
    e.tripUpdate.stopTimeUpdate = _.map(e.tripUpdate.stopTimeUpdate, u => {
      if (_.has(u.arrival, "time")) {
        const d = new Date(u.arrival.time);
        u.arrival.time = d.getTime();
        
      } 
      return u;
    });
    const d = new Date(e.tripUpdate.timestamp);
    e.tripUpdate.timestamp = d.getTime();
    ret.push(e);
  });
  return ret;
};

const Verify = (msgHandler, payload) => {
  const err = msgHandler.verify(payload);
  if (err) {
    console.error(err);
    return false;
  }
  return true;
};

const resetTripUdate = async gtfsFeedId => {
  log.info('Updating Arrival Estimation feed for ' + gtfsFeedId);
  const d = new Date();
  let entity = storeH.query(gtfsFeedId, 'TripUpdates');

  if (!entity) {
    log.error ('Nothing to update for ' + gtfsFeedId);
    return;
  }
  entity = convertDates(entity);
  
  const payload = {
    header: header,
    entity: entity
  };

  const msgH = tripUpdatesProto.lookupType("transit_realtime.FeedMessage");
  if (!Verify(msgH, payload)) {
    return;
  }
  
  const msg = msgH.create(payload);
  const buffer = msgH.encode(msg).finish();
  const fn = dirPath + gtfsFeedId;
  // write to file
  
  const ret = await fs.writeFile(fn, buffer).catch(err => {
    console.error(err);
    return;
  });
  log.info('GTFS-RT binary file saved');
};

const resetAllTripUpdates = async () => {
  const list = storeH.list('TripUpdates');
  for (elem of list) {
    // try {
      await resetTripUdate(elem);
    // } catch (e) {
    //   console.error (e)
    // }
  }
}

const clearTripUpdates = async () => {
  log.info ('Removing GTFS-RT binary files');
  const files = await fs.readdir(dirPath);
  for (const file of files) {
    await fs.unlink(path.join(dirPath, file));
  }
  return;
}

init(); 
module.exports = {
  resetTripUdate: resetTripUdate,
  resetAllTripUpdates: resetAllTripUpdates,
  clearTripUpdates: clearTripUpdates
};
