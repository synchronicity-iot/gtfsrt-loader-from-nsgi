// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const log = require('../utils/logger');
const storageH = require('../store/storeHandler');
const updateFeeds = require('../feeds/updateFeeds');
const prettyjson = require('prettyjson');
const fs = require('fs-extra');

module.exports = async (app) => {
  app.get('/remove', async (req, res) => {
    storageH.removeByTime(req.query.gtfsFeedId, req.query.feedName, req.query.time);
    return res.send(); 
  });

  

  app.get('/print', async (req, res) => {
    storageH.print(req.query.feedName);
    return res.send();
  });

  

  
}; 