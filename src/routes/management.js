// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const log = require('../utils/logger');
const _ = require("lodash");
const gtfsrtFeeds = require ('../utils/common').gtfsrtFeeds;
const ngsiSubs = require ('../utils/ngsiSubs');
const prettyjson = require('prettyjson');
const storageH = require('../store/storeHandler');
const updateFeeds = require('../feeds/updateFeeds');

const getFeedNames = (queryParams) => {
  // let feedNames = [];
  //   if (_.has(queryParams, 'feedNames')) {
  //     feedNames = queryParams.feedNames.split(',');
  //     feedNames = _.filter(feedNames, e => {
  //       log.debug('Checking ' + e)
  //       log.debug(gtfsrtFeeds)
  //       if (_.includes(gtfsrtFeeds, e)) {
  //         log.debug(e+' exist')
  //       }
  //       return _.includes(gtfsrtFeeds, e);
  //     });
  //   } else {
  //     feedNames = gtfsrtFeeds;
  //   } 
  //   return feedNames;
  // So far only TripUpdates (ArrivalEstimation)
  return ['TripUpdates'];
}

const postFeed = async (req, res) => {
  const feedNames = getFeedNames(req.query);
  for (const feed of feedNames) {
    await ngsiSubs.createSub(feed, req.params.gtfsFeedName)
  }
  return res.send();
}

const getFeed = async (req, res) => {
  const ret = storageH.query(req.params.gtfsFeedName, 'TripUpdates');
  return res.send(ret);
}

const deleteFeed = async (req, res) => {
  const time = req.query.time || '2000-01-01';
  const ret = storageH.removeByTime(req.params.gtfsFeedName, 'TripUpdates', time);
  return res.send(ret);
}

const loadFeed = async (req, res) => {
  updateFeeds.resetTripUdate(req.params.gtfsFeedName);
  return res.send();
}

const getFeedList = async (req, res) => {
  const list = storageH.list('TripUpdates');
  return res.send(list);
}

const getFeeds = async (req, res) => {
  const ret = storageH.queryAll('TripUpdates');
  return res.send(ret);
}

const deleteFeeds = async (req, res) => {
  await ngsiSubs.cleanSubs(); // remove subscriptions in ORION
  await storageH.removeAll('TripUpdates'); // free cache
  await updateFeeds.clearTripUpdates(); // remove binary files
  return res.send();
}

const loadFeeds = async (req, res) => {
  updateFeeds.resetAllTripUpdates();
  return res.send();
}

const notify = async (req, res) => {
  storageH.processNotification(req.body.data);
  return res.send();
} 

module.exports = async (app) => {
  
  app.post('/management/feed/:gtfsFeedName', postFeed);
  app.get('/management/feed/:gtfsFeedName', getFeed); 
  app.delete('/management/feed/:gtfsFeedName', deleteFeed); 
  app.post('/management/loadFeed/:gtfsFeedName', loadFeed); 
  
  app.get('/management/feedlist', getFeedList); 
  app.get('/management/feeds', getFeeds);   
  app.delete('/management/feeds', deleteFeeds); 
  app.post('/management/loadFeeds', loadFeeds); 

  app.post('/notify', notify); 
  
} 