// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const log = require('../utils/logger');
const _ = require("lodash");
const fs = require('fs-extra');

const getgtfsrt = async (req, res) => {
  log.debug ('Serving feed ' + req.params.gtfsFeedName);
  const buff = await fs.readFile('./feeds/TripUpdate/' + req.params.gtfsFeedName, 'binary').catch (err => {
    console.error(err)
  })
  if (!buff) {
    return res.status(404).send();
  }
  res.writeHead (200, {'Content-Type':'application/octet-stream'});
  return res.end(buff, 'binary');
}

module.exports = async (app) => {
  app.get('/gtfsrt/:gtfsFeedName', getgtfsrt);
}